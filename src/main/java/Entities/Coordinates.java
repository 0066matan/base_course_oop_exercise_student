package Entities;

import java.awt.geom.Point2D;

public class Coordinates {
    String name;
    int x;
    int y;

    public double findDistanceFromOtherPlace(Coordinates otherplace){
        return Point2D.distance(x,y,otherplace.x,otherplace.y);
    }

    public String name(){
        return this.name;
    }

    public String toString(){
        return ""+x+':'+y;
    }
}
