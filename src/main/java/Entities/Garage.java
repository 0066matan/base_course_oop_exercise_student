package Entities;

import AerialVehicles.AerialVehicle;

public class Garage {

    public void repair(AerialVehicle toRepair){
        toRepair.setFlightTimeFromLastFix(0);
        toRepair.changeStatus("Ready");
    }
}
