package AerialVehicles;

import AerialVehicles.MissionEquipment.Equipment;
import Entities.Garage;
import Entities.Coordinates;

public abstract class AerialVehicle {
  double flightTimeFromLastFix;
  String flightStatus;
  Coordinates homeBase;
  int maxFlightTime;
  Equipment planEquipment;

  public void setFlightTimeFromLastFix(double newTime) {
    this.flightTimeFromLastFix = newTime;
  }

  public void changeStatus(String newStatus) {
    this.flightStatus = newStatus;
  }

  public Coordinates homeBase() {
    return homeBase;
  }

  public Equipment checkEquipment(){
    return planEquipment;
  }

  public void flyTo(Coordinates destination) {
    switch (flightStatus) {
      case "Ready":
        System.out.println("Flying to: " + destination.name());
        setFlightTimeFromLastFix(
                this.flightTimeFromLastFix + homeBase.findDistanceFromOtherPlace(destination));
        changeStatus("Flying");
        break;
      case "Not Ready":
        System.out.println(("Not Ready to Fly"));
        break;
      case "Flying":
        System.out.println("Already Flying");
        break;
    }
  }

  public void land(Coordinates destination) {
    System.out.println("Landing on: " + destination.name());
    check();
  }

  public void check() {
    if (flightTimeFromLastFix > maxFlightTime) {
      changeStatus("Not Ready");
      new Garage().repair(this);
    } else {
      changeStatus("Ready");
    }
  }


}
