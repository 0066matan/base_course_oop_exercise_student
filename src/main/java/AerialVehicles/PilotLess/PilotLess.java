package AerialVehicles.PilotLess;

import AerialVehicles.AerialVehicle;
import AerialVehicles.MissionEquipment.IntelligenceEquipment;
import Entities.Coordinates;

public abstract class PilotLess extends AerialVehicle {
  IntelligenceEquipment intelligenceEquipment;

  public void hoverOverLocation(Coordinates destination) {
    this.changeStatus("Flying");
    System.out.println("Hovering Over: " + destination.toString());
  }
}
