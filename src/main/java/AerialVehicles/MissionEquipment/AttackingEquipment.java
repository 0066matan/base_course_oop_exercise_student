package AerialVehicles.MissionEquipment;

public class AttackingEquipment extends Equipment {
    private int numOfMissles;
    private String missleType;

    public int numOfMissles() {
        return numOfMissles;
    }

    public String missleType() {
        return missleType;
    }
}
