package Missions;

import AerialVehicles.MissionEquipment.IntelligenceEquipment;

public class IntelligenceMission extends Mission {
  @Override
  public void executeMission() {
    if (executingPlane.checkEquipment().getClass().equals(IntelligenceEquipment.class)) {
      IntelligenceEquipment missionEquipment = (IntelligenceEquipment) executingPlane.checkEquipment();
      System.out.println(
          pilotName
              + ':'
              + executingPlane.getClass().toString()
              + " collecting data in "
              + destination.name()
              + " with sensor type: "
              + missionEquipment.sensorType());
    }
  }
}
