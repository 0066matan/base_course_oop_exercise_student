package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    Coordinates destination;
    String pilotName;
    AerialVehicle executingPlane;

    public void begin(){
        System.out.println("Beginning Mission!");
        executingPlane.flyTo(destination);
    }

    public void cancel(){
        System.out.println("Aborting Mission!");
        executingPlane.land(executingPlane.homeBase());
    }

    public void finish() throws AerialVehicleNotCompatibleException {
        executeMission();
        executingPlane.land(executingPlane.homeBase());
    }

    public abstract void executeMission() throws AerialVehicleNotCompatibleException;
}
