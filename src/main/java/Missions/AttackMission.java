package Missions;

import AerialVehicles.MissionEquipment.AttackingEquipment;

public class AttackMission extends Mission {

  @Override
  public void executeMission() throws AerialVehicleNotCompatibleException {
    if (executingPlane.checkEquipment().getClass().equals(AttackingEquipment.class)) {
      AttackingEquipment missionEquipment = (AttackingEquipment) executingPlane.checkEquipment();
      System.out.println(
          pilotName
              + ':'
              + executingPlane.getClass().toString()
              + " Attacking suspect "
              + destination.name()
              + " with"
              + missionEquipment.missleType()
              + 'x'
              + missionEquipment.numOfMissles());
    } else {
      throw new AerialVehicleNotCompatibleException("Unfit equipment for attacking mission");
    }
  }
}
