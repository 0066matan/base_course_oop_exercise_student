package Missions;

import AerialVehicles.MissionEquipment.BdaEquipment;

public class BdaMission extends Mission {
  @Override
  public void executeMission() {
      if (executingPlane.checkEquipment().getClass().equals(BdaEquipment.class)) {
          BdaEquipment missionEquipment = (BdaEquipment) executingPlane.checkEquipment();
    System.out.println(
        pilotName
            + ':'
            + executingPlane.getClass().toString()
            + " taking pictures of suspect "
            + destination.name()
            + " with "
            + missionEquipment.cameraType() +
            " camera.");
  }}
}
